Ensemble of Gene Set Enrichment Analyses R Package
 
Author: Monther Alhamdoosh, Milica Ng and Matthew Ritchie 
E:m.hamdoosh@gmail.com

You can download the current stable release from this url

https://bitbucket.org/malhamdoosh/egsea/get/Stable_Release.tar.gz

Alternatively, you can install EGSEA in your local computer by typing these commands in the R console
library(devtools)

install_bitbucket("malhamdoosh/egseadata", ref="Stable_Release")

install_bitbucket("malhamdoosh/egsea", ref="Stable_Release")

Note that devtools should be installed before you can run these commands.

In order to get the development version of EGSEA, use ref = "Devel_Release". 